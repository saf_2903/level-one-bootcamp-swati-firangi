//Write a program to find the volume of a tromboloid using one function

#include<stdio.h>
int main()
{
  float h, b, d, volume;
  printf("Enter h, b, d of the tromboloid.\n");
  scanf("%f %f %f", &h,&b,&d);
  volume= (1/(3*b))* ((h*d)+d);
  printf("Volume of Tromboloid is %f. \n",volume);
  return 0;
}
  